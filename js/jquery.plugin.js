
(function($) {
	$.fn.spin = function(options) {

		var settings = $.extend({
			src: 'http://www.walterlogeman.com/art/images/2008-07-01-circles/reg/circle2.jpg',
			spinFoward: true,
			height: '50px',
			width: '50px',
			d: 0
		}, options);

		return this.each( function() {
			var img = document.createElement("img");
			img.setAttribute('src', settings.src);
			img.setAttribute('height', settings.height);
			img.setAttribute('width', settings.width);
			$(this).append(img);

			spin = function(){
				//alert(this.d);
				var that = this;
				img.style.transform = "rotate(" + settings.d + "deg)";
				img.style.WebkitTransform= "rotate(" + settings.d + "deg)";

				//alert(this.spinFoward);
				if (settings.spinFoward == true){
					settings.d++;
				}else{
					settings.d--;
				}
			}
			setInterval(spin, 100);
		});
	}
}(jQuery));

