// JavaScript Document

function sessionCheck(successFunction){
	alert("checking if you have a session");

	$.ajax({
		url: "server/requestSession.php",
		type: "GET",

		success: function (response){
			successFunction(response);
			return response	;
		},

		error: function(){
			return false;
		}
	});
}
