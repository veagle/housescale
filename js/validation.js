
(function($) {
	"use strict";

	$.fn.validation = function(options){

		options = options || {};

		var settings = $.extend({
			username_f_ID: "nam",
			password_f_ID: "pw",
			email_f_ID: "email",
			retypePW_f_ID: "confirmPW",

			username_error: "#username-error-box",
			password_error: "#password-error-box",
			email_error: "#email-error-box",
			retype_error: "#retype-error-box"
		}, options);

		var $form = this;
		var filters = {
			username:{
				regex: /^[a-z|A-Z|0-9|\_]{3,}$/,
				error: "username: at least 4 characters long, includes '_'"
			},

			password:{
				regex: /^[A-Z|a-z|0-9]{6,}$/,
				error:	"password: at least 6 characters long"
			},

			email:{
				regex: /^[A-Z|a-z|0-9|\_]{1,}\@[A-Z|a-z|0-9|\_]{3,}(.com)/,
				error:	"please provide a valid email"
			}
		}

		var testingAndError = function(regex, value, error_msg, error_msg_field_id){
			var pattern = new RegExp(regex);
			if (!pattern.test(value)){
				var ele = document.createElement("p");
				ele.textContent = error_msg;
				ele.setAttribute('style', 'color:white;position:absolute;top:-15px;left:8px;font-size:15px;font-weight:bold;');

				jQuery(error_msg_field_id).append(ele);
				jQuery(error_msg_field_id).show();
			}
		}

		var $inputs = $form.find(":input");
			$inputs.each(function() {
				var pattern;
				if(this.id == settings.username_f_ID){
					testingAndError(filters.username.regex, this.value, filters.username.error, settings.username_error);
				}else if (this.id == settings.password_f_ID){
					testingAndError(filters.password.regex, this.value,  filters.password.error, settings.password_error );
				}else if (this.id == settings.email_f_ID){
					testingAndError(filters.email.regex, this.value,  filters.email.error, settings.email_error );
				}else if (this.id == settings.retypePW_f_ID){
					//to do
				}
			});
		}
	return this;

}(jQuery));