// JavaScript Document

//img, a <img> element
//address, a  <p> element
//rate is an array of ranking number from 0-5, {price, location, quality}
function createHouse(img, add, rate){
	this.image=img;
	this.address=add;
	this.rating=rate;
}

createHouse.prototype.getImage = function(){
	return this.image;
}

createHouse.prototype.getAddress = function(){
	return this.address;
}

createHouse.prototype.getRating = function(){
	return this.rating;
}