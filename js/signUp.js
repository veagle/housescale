//Ajax send form to server
	function signUp(){

		//reset previous errors
		$("#username-error-box").hide();
		$("#password-error-box").hide();
		$("#retype-error-box").hide();
		$("#email-error-box").hide();
		document.getElementById("hintBox").innerHTML = "";

		//check if there is any input that violate the rules, if so it will print out in the error fields
		$("#Signup").validation({
			username_f_ID: "name",
			password_f_ID: "pw",
			email_f_ID: "email",
			retypePW_f_ID: "confirmPw"
		});

		var password = document.getElementById('pw').value;
		var confirmPW = document.getElementById('confirmPw').value;

		//make sure password and retype password is the same
		if(password != confirmPW){
			$("#retype-error-box").show();
			document.getElementById("retype-error-p").innerHTML = "retype password is wrong buddy";
		}

		if ($("#username-error-box, #password-error-box, #retype-error-box, #email-error-box").is(":visible")){
			return false;
		}
		sendRequest();
	}

	function sendRequest(){
			var xmlhttp;
			var getString;
			var url = "server/signUp.php";
			var username = document.getElementById('name').value;
			var password = document.getElementById('pw').value;
			var confirmPW = document.getElementById('confirmPw').value;
			var email = document.getElementById('email').value;
			var response = Recaptcha.get_response();
			var challenge = Recaptcha.get_challenge();

			url = url + "?username=" + username + "&password="
				+ password + "&email=" + email + "&challenge="
				+ challenge + "&response=" + response;

			if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.open("get", url , true);
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


			xmlhttp.onreadystatechange = function(){
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200){

						alert("reached inside");
						getString = $.parseJSON(xmlhttp.responseText);
						Recaptcha.reload();

						if (getString.error == 0){
							alert(getString.msg);
							window.location = "http://localhost/start/houseScale/index.html";
						} else{
							errorFromServer(getString);
						}
				}
			}
			xmlhttp.send();
			resetForms();
		}

	function errorFromServer(array){
		for (var i in array.msg){
			//here we need to check if the register went ok, if not we got to show error on hintbox
			//if successful we redirect user to index page
			if (array.msg[i] == "username already taken"){
				$("#username-error-box").show();
				document.getElementById("user-error-p").innerHTML = "username taken already, please pick another name";
			}else if(array.msg[i] == "wrong CAPTCHA"){
				document.getElementById("hintBox").innerHTML = "captcha is wrong, please try again";
			}else if(array.msg[i] == "email already taken"){
				$("#email-error-box").show();
				document.getElementById("email-error-p").innerHTML = "email taken already";
			}
		}
	}

	function resetForms(){
		document.getElementById('Signup').reset();
	}