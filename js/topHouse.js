
(function getTopHouseFromServer(){
	//Connecting to server
	var xmlhttp;
	var getArray;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	var url = "server/topHouses.php";
	xmlhttp.open("POST", url, true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
			//Storing response from server, an array encoded by json
			getArray = $.parseJSON(xmlhttp.responseText);

			//topHouses: an array that contain all the top houseObjects
			var topHouses = houseObjectCreate(getArray);
			createSlideShow(topHouses);
		}
	}
	xmlhttp.send();
})();

//array: key-value array that contain all the image, addresses, ratings that were received by server, already decoded into javascript
function houseObjectCreate(array){

	var img = new Array(); //storing the image elements that contain the house image
	var para = new Array(); //storing the paragraph element that contain the address
	var topHouse = new Array(); //an array storing all of the house object created base on data received from server

	//creating house objects base on data received from server
	for (var i = 0; i < array.length; i++){
		//creating the image element
		img[i] = document.createElement("img");
		img[i].setAttribute('src', array[i].image);
		img[i].setAttribute('height', '160px');
		img[i].setAttribute('width', '150px');

		//creating paragraph element for putting in the address
		para[i] = document.createElement("p");
		var node = document.createTextNode(array[i].address);
		para[i].appendChild(node);

		//creating a house object and putting it into an array
		topHouse[i] = new createHouse(img[i], para[i], array[i].rating);

	}//topHouse object array now contain all the topHouse informations

	return topHouse;
}





function createSlideShow(topHouse){


	//NOTE: use cloneNode for deep copy
	//getting the div by id, to attach elements to them later on
	var imgElement=document.getElementById("image1");
	var addElement=document.getElementById("address");
	var prevDiv = document.getElementById("preview");
	var ratingElement = document.getElementById("rating");


	//set up the images, address, rating and thumbnails onto the page. Style and attribute are done here
	createImagesAndAddress(topHouse, imgElement, addElement, ratingElement);
	createRating(topHouse, ratingElement);
	createThumbnails(topHouse, prevDiv);

	//set up the actual animation for the slideShow
	slideShowAnimation(topHouse);

}



//topHouse: array that contains a list of top house objects
//imgElement, addElement: the div element that the function will create the images and address of houses in
function createImagesAndAddress(topHouse, imgElement, addElement){
	//put the houses images and address into the div, and setting the style
	for(var i=0; i<topHouse.length;i++){
		var houseImageEle = topHouse[i].getImage();
		var houseAddEle = topHouse[i].getAddress();
		//image and address
		houseImageEle.setAttribute('style', 'position:absolute; top:0px; display:none; -moz-border-radius-topleft: 15px; -moz-border-radius-topright:15px; -moz-border-radius-bottomright:15px; -moz-border-radius-bottomleft: 15px;-webkit-border-top-left-radius: 15px;-webkit-border-top-right-radius: 15px;-webkit-border-bottom-left-radius: 15px;-webkit-border-bottom-right-radius: 15px;');
		houseImageEle.setAttribute('id', 'house'+i);
		imgElement.appendChild(houseImageEle);
		houseAddEle.setAttribute('style', 'position:absolute;top:20px;left:0px;display:none;white-space:nowrap');
		houseAddEle.setAttribute('id', 'address'+i);
		addElement.appendChild(houseAddEle);
	}
}



//topHouse: array that contains a list of top house objects
//ratingElement: the div element that the function will create the ratings in
function createRating(topHouse, ratingElement){
	for(var i=0; i<topHouse.length;i++){
		var list = topHouse[i].getRating();
		var rateId = "ranking"+i;
		drawHouses(list[0], list[1], list[2], list[3], rateId, ratingElement);
	}
}


//topHouse: array that contains a list of top house objects
//prevDiv: the div element that the function will create the thumbnails in
function createThumbnails(topHouse, prevDiv){
	//preview, the thumbnails
	for(var i=0; i<topHouse.length;i++){

		var length = topHouse.length;
		var size = 120 / 3;
		var left = -65; //each image +5

		var prev = topHouse[i].getImage().cloneNode(false);
		prev.setAttribute('style','position:absolute;opacity:0.4;border-radius:150px;-webkit-border-radius: 150px;-moz-border-radius: 150px;');
		prev.setAttribute('height', size);
		prev.setAttribute('width', size);
		prev.setAttribute('id', 'thumb'+i);
		prev.style.left='-70px';
		var numSum = 145 + (size+5)*i; //each image -15, -5
		var num = numSum +'px';
		prev.style.top=num;
		prevDiv.appendChild(prev);
	}
}



//rateId is actually the class name. Id doesnt work in this case
function drawHouses(rent, location, quality, owner, rateId, divAttach){
	var positionLeft = 620;

	//creating a image element of the house symbol
	var img = document.createElement("img");
	img.setAttribute("src", "http://icons.iconarchive.com/icons/double-j-design/origami-colored-pencil/256/blue-home-icon.png");
	img.setAttribute('height', '20px');
	img.setAttribute('width', '20px');
	img.setAttribute('class', rateId);

	//rent rating, start cloning the right amount of house symbol and attach it to the class, rateId
	for (var ii = 0; ii<rent; ii++){
		var clone = img.cloneNode(false);
		clone.setAttribute('style', 'position:absolute;top:35px;display:none');
		clone.setAttribute('class', rateId);
		var numSum = positionLeft + ii*20;
		var num = numSum + 'px';
		clone.style.left=num;
		divAttach.appendChild(clone);


	}
	//location rating, start cloning the right amount of house symbol and attach it to the class, rateId
	for (var ii = 0; ii<location; ii++){
		var clone = img.cloneNode(false);
		clone.setAttribute('style', 'position:absolute;top:66px;display:none');
		clone.setAttribute('class', rateId);
		var numSum = positionLeft + ii*20;
		var num = numSum + 'px';
		clone.style.left = num;
		divAttach.appendChild(clone);

	}
	//quality rating, start cloning the right amount of house symbol and attach it to the class, rateId
	for (var r = 0; r<quality; r++){
		var clone = img.cloneNode(false);
		clone.setAttribute('style', 'position:absolute;top:99px;display:none');
		clone.setAttribute('id', rateId);
		var numSum = positionLeft + r*20;
		var num = numSum + 'px';
		clone.style.left=num;
		divAttach.appendChild(clone);
	}

	//Owner rating, start cloning the right amount of house symbol and attach it to the class, rateId
	for (var rr = 0; rr<owner; rr++){
		var clone = img.cloneNode(false);
		clone.setAttribute('style', 'position:absolute;top:132px;display:none');
		clone.setAttribute('id', rateId);
		var numSum = positionLeft + rr*20;
		var num = numSum + 'px';
		clone.style.left = num;
		divAttach.appendChild(clone);
	}

}

//actual sliding animation
//called after all the images, addresses, ratings and thumbnails are received from server and has been set up onto the page
  function slideShowAnimation(topHouse){
	  var counter = {
			  index:0
	  }


	  $(document).ready(function(){


		  $("#house0").fadeIn();
		  $("#address0").fadeIn();
		  $(".ranking0").fadeIn();
		  $("#thumb0").css({"opacity":"1.0"});


		  //Switching houses using next button
		  $("#next").click(function(){
			  if(counter.index==topHouse.length-1){
				  counter.index=0;
			  }else{
				  counter.index++;
			  }
				  switchHouse(counter.index);

		  });

		  //Switching houses using previous button
		  $("#previous").click(function(){
			  if(counter.index==0){
				  counter.index=topHouse.length-1;
			  }else{
				  counter.index--;
			  }
			  switchHouse(counter.index);
		  });


		  //Switching houses via clicking thumbnails
		  for (var i=0; i<topHouse.length; i++){
			  var index = counter.index;
			  $("#thumb"+i).click((function(index) {
				  return function() {
					  switchHouse(index);
				  }

			  })(i));
		  }



	  });


	  function switchHouse(i){
			  fadeOutAll();
			  counter.index = i;
			  $("#house"+i).fadeIn(800);
			  $("#address"+i).fadeIn(800);
			  $(".ranking"+i).fadeIn(800);
			  $("#thumb"+i).css({"opacity":"1.0"});

	  }

	  function fadeOutAll(){
		  for(var i=0; i<topHouse.length; i++){
			  $("#house"+i).fadeOut();
			  $("#address"+i).fadeOut();
			  $(".ranking"+i).fadeOut();
			  $("#thumb"+i).css({"opacity":"0.4"});
		  }

	  }

  }  // JavaScript Document