
(function(){
	//sessionCheck.js
	//jquery ajax
	sessionCheck(whichBarToDisplay);
})();

//taking in the response from server. If receive a username, display the user logged in bar, if not then display the log in bar
function whichBarToDisplay(response){
		//callback function, when entered this mean we already got a response from server
		//therefore display userbar div
		$(".user-bar").show();
		alert("this is response " + response);
		//check if user is logged in already or not
		if (! (response == "") ){
			$(".logIn").hide();
			document.getElementById("welcomeUserText").innerHTML= "Welcome back" + response;
			$("#welcomeUserText").show();
		}else{
			$("#welcomeUserText").hide();
			$(".logIn").show();
		}
}

    $(document).ready(function(){
		$("#diagonalLine").show(500);
    	$("#line").show(1300);
		$("#start").show(1500);

        $("#centerButton").click(function(){
			$("#diagonalLine").hide(1500);
			$("#line").hide(1300);
			$("#start").hide(500);
            fadeInWaterloo();
            fadeInToronto();
            fadeInTop();
			$("#selectLine").animate({width:'toggle'},1250);;
			$("#diagonalSelectLine").show(1450);
			$("#select").show(1600);
        });

        $("#waterloo7").mouseenter(function(){
            fadeInTorontoStop();
			fadeInTopStop();
			fadeOutToronto();
            fadeOutTop();
        });

        $("#toronto5").mouseenter(function(){
            fadeInWaterlooStop();
			fadeInTopStop();
            fadeOutWaterloo();
            fadeOutTop();
        });

        $("#campus").mouseenter(function(){
			fadeInWaterlooStop();
			fadeInTorontoStop();
			fadeOutWaterloo();
            fadeOutToronto();
        });

        $("#waterloo7").mouseleave(function(){
			fadeOutTorontoStop();
			fadeOutTopStop();
            fadeInToronto();
            fadeInTop();
        });

        $("#toronto5").mouseleave(function(){
            fadeInWaterloo();
            fadeInTop();
        });

        $("#campus").mouseleave(function(){
            fadeInToronto();
            fadeInWaterloo();
        });

		$("#log-in-button").click(function(){sendLogin(); return false;});
		$("#forgotLogin").click(function(){location.href='signUp.html';});
		$("#signUp").click(function(){location.href='signUp.html';});
    });

	//-------------------------------------Toronto Animation Functions--------------------------
	function fadeInToronto(){
		for (var i=1;i<=5; i++){
			$("#toronto"+i).fadeIn(300+i*200);
		}
    }

    function fadeOutToronto(){
		for (var i=1; i<=5; i++){
        	$("#toronto"+i).fadeOut(1100-i*200);
		}
    }

	function fadeInTorontoStop(){
			$(".toronto").fadeIn().stop();
    }

    function fadeOutTorontoStop(){
        	$(".toronto").fadeOut().stop();

    }
	//-----------------------------------End of Toronto Animation---------------------------


	//-----------------------------------Waterloo Animation Functions----------------------------
    function fadeInWaterloo(){
		$("#waterloo1").fadeIn(0);
        $("#waterloo2").fadeIn(300);
        $("#waterloo3").fadeIn(1300);
        $("#waterloo4").fadeIn(1600);
        $("#waterloo5").fadeIn(1800);
        $("#waterloo6").fadeIn(2000);
        $("#waterloo7").fadeIn(2000);
    }

	function fadeInWaterlooStop(){
			$(".waterloo").fadeIn().stop();
	}

    function fadeOutWaterloo(){
		var num = 1100;
		for(var i = 1; i <= 7; i++){
			$("#waterloo" + i).fadeOut(num);
			num = num - 200;
		}
    }

	function fadeOutWaterlooStop(){
			$(".waterloo").fadeOut().stop();
	}
    //----------------------------------------End of Waterloo Animation-------------------


	//-------------------------------------Campus Animation Functions--------------------
    function fadeInTop(){
		for (var i = 1; i <= 4; i++){
        	var id = "#top"+i;
			$(id).fadeIn(300+i*200);
		}
        $("#campus").fadeIn(1100);
    }

    function fadeOutTop(){
        $("#top1").fadeOut(1100);
        $("#top2").fadeOut(1100);
        $("#top3").fadeOut(900);
        $("#top4").fadeOut(700);
        $("#campus").fadeOut(500);
    }

    function fadeInTopStop(){
		$(".top").fadeIn().stop();
    }

	function fadeOutTopStop(){
        $(".top").fadeOut().stop();
    }
    //-------------------------------------End of Campus Animation---------------


	function sendLogin(){

		var xmlhttp;
		var getString;
		var url = "server/login.php";

		var username = document.getElementById('name').value;
		var password = document.getElementById('pw').value;
		url = url + "?username=" + username + "&password=" + password;

		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		}else{// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		xmlhttp.open("get", url , true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.onreadystatechange = function(){
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
					getString = xmlhttp.responseText;
					alert(getString);
					$(".logIn").hide();
					document.getElementById("welcomeUserText").innerHTML="Welcome back Veagle";
					$("#welcomeUserText").show();
					//if we get back it is correct login msg, should redirect to a page that say "you have logged in" then redirect back
			}
		};
		xmlhttp.send();
	}
    // JavaScript Document