

	//Connecting to server
	var xmlhttp;
	var getString;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
  
	var url="server/topHouses.php";
	xmlhttp.open("POST", url, false);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			
			//Storing response from server, an array encoded by json	
			getString = $.parseJSON(xmlhttp.responseText);
			
			/////////////////////////////////////////////////////////////fix	
			var img1 = document.createElement("img");
			var img2 = img1.cloneNode(false);
			var img3 = img1.cloneNode(false);
			
			img1.setAttribute('src', getString.image1);
			img1.setAttribute('height', '160px');
			img1.setAttribute('width', '150px');
			
			img2.setAttribute('src', getString.image2);
			img2.setAttribute('height', '160px');
			img2.setAttribute('width', '150px');
			
			img3.setAttribute('src', getString.image3);
			img3.setAttribute('height', '160px');
			img3.setAttribute('width', '150px');
			
			var br = document.createElement("br");
			var br2 = br.cloneNode(false);
			var para=document.createElement("p");
			var nodee=document.createTextNode(getString.address1);
			var nodeee=document.createTextNode("Waterloo, Ontario");
			para.appendChild(br);
			para.appendChild(nodee);
			para.appendChild(br2);
			para.appendChild(nodeee);
			
			var br = br.cloneNode(false);
			var para2=document.createElement("p");
			var node22=document.createTextNode(getString.address2);
			para2.appendChild(br);
			para2.appendChild(node22);
			
			var br = br.cloneNode(false);
			var para3=document.createElement("p");
			var node33=document.createTextNode(getString.address3);
			para3.appendChild(br);
			para3.appendChild(node33);
			
			var house = new createHouse(img1, para, getString.rating1);
			var house1 = new createHouse(img2, para2, getString.rating2);
			var house2 = new createHouse(img3, para3, getString.rating3);
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			var topHouse = new Array();
			//this should be updated everyday at a certain time using setInterval()
			//get houseObjects from server and put it into the array
			//to add in more house just do topHouse[x] = housex; without the need to changing anything below
			topHouse[0] = house;
			topHouse[1] = house1;
			topHouse[2] = house2;
			
			
			//NOTE: use cloneNode for deep copy
			//getting the div by id, to attach elements to them later on
			var imgElement=document.getElementById("image");
			var addElement=document.getElementById("address");
			var prevElement = document.getElementById("preview");
			var ratingElement = document.getElementById("rating");
			
			
			
			//put the houses into the div, and setting the style
			for(var i=0; i<topHouse.length;i++){
				
				//image and address
				topHouse[i].getImage().setAttribute('style', 'position:absolute;top:0px;display:none;-moz-border-radius-topleft: 15px;-moz-border-radius-topright: 15px;-moz-border-radius-bottomright: 15px;-moz-border-radius-bottomleft: 15px;-webkit-border-top-left-radius: 15px;-webkit-border-top-right-radius: 15px;-webkit-border-bottom-left-radius: 15px;-webkit-border-bottom-right-radius: 15px;');
				topHouse[i].getImage().setAttribute('id', 'house'+i);
				imgElement.appendChild(topHouse[i].getImage());
				topHouse[i].getAddress().setAttribute('style', 'position:absolute;top:20px;left:0px;display:none;white-space:nowrap');
				topHouse[i].getAddress().setAttribute('id', 'address'+i);
				addElement.appendChild(topHouse[i].getAddress()); 
				
				//rating
				var list = topHouse[i].getRating();
				var rateId = "ranking"+i;
				drawHouses(list[0], list[1], list[2], list[3], rateId);
			}
			
			
			//preview, the thumbnails
			for(var i=0; i<topHouse.length;i++){
				var prev = topHouse[i].getImage().cloneNode(false);
				prev.setAttribute('style', 'position:absolute;left:-70px;width:40px;height:40px;opacity:0.4;border-radius: 150px;-webkit-border-radius: 150px;-moz-border-radius: 150px;');
				prev.setAttribute('id', 'thumb'+i);
				var numSum = 160 + 50*i; 
				var num = numSum +'px';
				prev.style.top=num;
				prevElement.appendChild(prev);
			}
			
			
			
			
			////////////////actual sliding animation///////////////////
			var index=0;
			$(document).ready(function(){
				
				
				$("#house0").fadeIn();
				$("#address0").fadeIn();
				$(".ranking0").fadeIn();
				$("#thumb0").css({"opacity":"1.0"});
				
				
				//Switching houses using next button
				$("#next").click(function(){	
					if(index==2){
						index=0;	
					}else{
						index++;
					}
						switchHouse(index);
						
				});
				
				//Switching houses using previous button
				$("#previous").click(function(){	
					if(index==0){
						index=2;	
					}else{
						index--;
					}
					switchHouse(index);
				});
				
				
				//Switching houses via clicking thumbnails
				$("#thumb0").click(function(){
					index=0;
					switchHouse(index);
				});
				$("#thumb1").click(function(){
					index=1;
					switchHouse(index);
				});
				$("#thumb2").click(function(){
					index=2;
					switchHouse(index);
				});
				
				
			});
			

			function switchHouse(i){
					fadeOutAll();
					$("#house"+i).fadeIn(800);
					$("#address"+i).fadeIn(800);
					$(".ranking"+i).fadeIn(800);
					$("#thumb"+i).css({"opacity":"1.0"});				
				
			}
			
			function fadeOutAll(){
				for(var i=0; i<topHouse.length; i++){
					$("#house"+i).fadeOut();
					$("#address"+i).fadeOut();
					$(".ranking"+i).fadeOut();
					$("#thumb"+i).css({"opacity":"0.4"});
				}
				
			}
			
			//rateId is actually the class name. Id doesnt work in this case
			function drawHouses(rent, location, quality, owner, rateId){
				
				//creating a image element of the house symbol
				var img = document.createElement("img");
				img.setAttribute("src", "http://icons.iconarchive.com/icons/double-j-design/origami-colored-pencil/256/blue-home-icon.png");
				img.setAttribute('height', '20px');
				img.setAttribute('width', '20px');
				img.setAttribute('class', rateId);
				
				//rent rating, start cloning the right amount of house symbol and attach it to the class, rateId
				for (var ii = 0; ii<rent; ii++){
					var clone = img.cloneNode(false);
					clone.setAttribute('style', 'position:absolute;top:35px;display:none');
					clone.setAttribute('class', rateId);
					var numSum = 620 + ii*20; 
					var num = numSum + 'px';
					clone.style.left=num;
					ratingElement.appendChild(clone);
					
				
				}
				//location rating, start cloning the right amount of house symbol and attach it to the class, rateId
					for (var ii = 0; ii<location; ii++){
					var clone = img.cloneNode(false);
					clone.setAttribute('style', 'position:absolute;top:66px;display:none');
					clone.setAttribute('class', rateId);
					var numSum = 620 + ii*20; 
					var num = numSum + 'px';
					clone.style.left = num;
					ratingElement.appendChild(clone);
				
				}
				//quality rating, start cloning the right amount of house symbol and attach it to the class, rateId
					for (var r = 0; r<quality; r++){
					var clone = img.cloneNode(false);
					clone.setAttribute('style', 'position:absolute;top:99px;display:none');
					clone.setAttribute('id', rateId);
					var numSum = 620 + r*20; 
					var num = numSum + 'px';
					clone.style.left=num;
					ratingElement.appendChild(clone);
				}
				
				//Owner rating, start cloning the right amount of house symbol and attach it to the class, rateId
					for (var rr = 0; rr<owner; rr++){
					var clone = img.cloneNode(false);
					clone.setAttribute('style', 'position:absolute;top:132px;display:none');
					clone.setAttribute('id', rateId);
					var numSum = 620 + rr*20; 
					var num = numSum + 'px';
					clone.style.left = num;
					ratingElement.appendChild(clone);
				}
				
			}

		}
	}
	
	xmlhttp.send("needs=topHouses");
	
	
